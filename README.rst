Telegram Sign-Up Robot
======================

Allow a group of users to sign up for events in a telegram chat.
Each chat can handle one or more events at a time.

Runs mostly as a standalone telegram bot app, but has been developed
using the Django ORM to manage databases, and uses the Django admin
page.  Since all of the business logic is in the model file, it
should be easy to extend this to a web based application in the
future.

General user commands::

    /signup show [<event>] -- brief info on all events or detailed information on an event
    /signup add <event> [<comment>]
    /signup remove <event>

Event manager commands::

    /event show [<event>] -- show brief list of all events or detailed information about an event
    /event create <event> <description...> -- create a new event with a description
    /event set-description <event> <description...> -- update the event description
    /event set-time <event> <date range> -- set a start and end time for the event
    /event pause <event> -- temporarily disable new signups
    /event active <event> -- re-enable signups on a paused event
    /event delete <event> -- delete this event
    /event add <event> <username> [<comment>] -- add user to an event
    /event remove <event> <username> -- remove user from an event

Bot manager commands (hidden)::

    /auth group - request that anyone in this group may create events
    /auth owner - request that you be given permission to make events in any group
    /auth master - request that you be given permission to approve owners
    /auth iamspartacus - set yourself as the first master
    /bless <username> - bless a user's request
