"""
Application

We run as a django app, but we still have an application so database migrations
do the right thing.
"""

from django.apps import AppConfig


class SignupBotConfig(AppConfig):
    """Register the application as if it was a standard one"""
    name = 'signupbot'
