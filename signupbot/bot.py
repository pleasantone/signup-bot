"""
Actual telegram bot
"""
import inspect
import logging
from datetime import timedelta
from functools import wraps

import humanize
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone
from telegram.ext import CommandHandler, Updater
from telegram import ParseMode
from timestring import Date, TimestringInvalid

from .models import Attendee, Chat, Event, TGLocation, TGUser, UserRequest, PermissionDenied
from .mwt import MWT

logger = logging.getLogger(__name__)        # pylint: disable=invalid-name


def _name(obj):
    return "{} {}".format(obj.first_name, obj.last_name).strip()


def log_cmd(function):
    @wraps(function)
    def tg_log(bot, update, args=''):
        """Wrap the function, log the run time and name of function"""
        title = (update.effective_chat.title
                 or _name(update.effective_chat)
                 or update.effective_chat.id)
        user = (update.effective_user.username
                or _name(update.effective_user)
                or update.effective_user.id)
        logger.debug("%s %s/%s: %s", function.__name__, title, user, " ".join(args))
        return function(bot, update, args=args)
    return tg_log


def _get_user(update):
    user = update.effective_user
    entry, _ = TGUser.objects.update_or_create(user_id=user.id)
    return entry.update(username=user.username, first_name=user.first_name,
                        last_name=user.last_name, language_code=user.language_code)


def _get_chat(update):
    chat = update.effective_chat
    entry, _ = Chat.objects.update_or_create(chat_id=chat.id)
    return entry.update(title=chat.title, first_name=chat.first_name,
                        last_name=chat.last_name, username=chat.username,
                        type=chat.type, invite_link=chat.invite_link)


def job_mwt_cleanup(unused_bot, unused_job):
    """Clean up memozie caches so they don't grow unbounded"""
    MWT().collect()


@MWT(timeout=60)
def _get_admin_ids(bot, chat_id):
    """Return a list of the admin IDs for a given chat. Results are cached for 1 minute"""
    return [admin.user.id for admin in bot.get_chat_administrators(chat_id)]


def _is_chat_admin(update):
    """Is the current user an administrator of the current chat?"""
    chat = update.effective_chat
    if chat.type == 'private' or chat.all_members_are_administrators:
        return True
    return update.effective_user.id in _get_admin_ids(chat.bot, chat.id)


def _show_event(event, verbose=True):
    count = event.attendee_set.all().count()
    msg = ["<b>{}</b>: {}".format(event.slug, event.title or "")]
    if event.start_time:
        msg.append(" starting: {:%a %x %I:%M%p}{}".format(
            event.start_time,
            ", for {}".format(humanize.naturaldelta(event.duration))
            if event.duration else ""))
    for line in event.description.split("\n"):
        msg.append("    " + line)
    msg.append(" {} attending, created by {}{}".format(
        count, event.owner,
        " ({})".format(event.state) if event.state != 'active' else ""))
    if verbose:
        for index, attendee in enumerate(event.attendee_set.all()):
            msg.append("    {}: {}{}".format(
                index+1, attendee.user,
                ": <i>{}</i>".format(attendee.comment) if attendee.comment else ""))
    return "\n".join(msg)


def _show_events(update, name=None, verbose=False):
    if name:
        verbose = True
        if name == 'all':
            name = None
    chat = _get_chat(update)
    if name:
        events = chat.event_set.filter(slug__iexact=name)
    else:
        events = chat.current_events()
    msg = [_show_event(event, verbose=verbose) for event in events]
    if not name:
        if msg:
            msg.insert(0, "Events:")
        else:
            msg = ["No active events."]
    elif not msg:
        msg = ["{} does not exist".format(name)]
    return update.message.reply_text("\n\n".join(msg), parse_mode=ParseMode.HTML,
                                     disable_web_page_preview=True, disable_notification=True,
                                     quote=False)


@log_cmd
def cmd_event(unused_bot, update, args):
    """usage: /event <command> ...

     *show* \[_<event>_] -- show brief list of all events or detailed information about an event
     *addme* _<event>_ \[_<comment>_]
     *removeme* _<event>_

    For event administration commands, use */event help admin*.
    """
    admin_help = """
        Event administrator commands (you must own the event to change it):

        *create* _<event>_ _<title...>_ -- create a new event
        *delete* _<event>_ -- delete the event
        *time* _<event>_ _<date and time>_ -- set start of event
        *title* _<event>_ _<title...>_ -- update the event title
        *info* _<event>_ _<description...>_ -- update the event description
        *pause* _<event>_ -- disable new signups
        *resume* _<event>_ --re-enable signups on a paused event
        *adduser* _<event>_ _<username>_ \[_<comment>_] -- add username to an event
        *removeuser* _<event>_ _<username>_ -- remove username from an event

        *NOTE:* To add a username to an event as an administrator, the user
        must have previously interacted with the bot.
        """
    try:
        command = args.pop(0).lower()
    except IndexError:
        command = None

    if not command:
        command = 'show'

    if command in ['help', '?', '--help']:
        if args and args[0].lower() == 'admin':
            help_text = inspect.cleandoc(admin_help)
        else:
            help_text = inspect.getdoc(cmd_event)
        return update.message.reply_text(help_text, parse_mode=ParseMode.MARKDOWN,
                                         disable_notification=True, quote=False)

    user = _get_user(update)
    chat = _get_chat(update)

    try:
        slug = args.pop(0).lower()
    except IndexError:
        slug = None

    if command == 'show':
        return _show_events(update, slug)

    if not slug:
        return update.message.reply_text("{} needs an event name".format(command))

    response = None
    try:
        if command == 'create':
            event = Event.add(chat, slug, user, title=" ".join(args))
            response = "{} created".format(event.slug)
        else:
            event = Event.objects.get(chat=chat, slug=slug)
            if command == 'addme':
                if event.state == 'active':
                    comment = " ".join(args)
                    Attendee.add(event=event, user=user, comment=comment)
                    response = "{} added {}{}".format(
                        event.slug, user, " {}".format(comment) if comment else "")
                else:
                    response = "{} is not accepting sign-ups at this time".format(event.slug)

            elif command == 'removeme':
                event.attendee_set.filter(user=user).delete()
                response = "{} removed from {}".format(user, event.slug)

            elif command == 'title':
                event.update(user, title=" ".join(args))

            elif command == 'info':
                event.update(user, description=" ".join(args))

            elif command == 'time':
                if not args:
                    response = "{} needs a start date & time".format(command)
                else:
                    start = Date(" ".join(args), tz=timezone.get_current_timezone())
                    event.update(user, start_time=start.date)

            elif command == 'delete':
                event.remove(user)
                response = "{} deleted".format(event.slug)

            elif command in ['pause', 'resume']:
                event.update(user, state='active' if command == 'resume' else command)

            elif command in ['adduser', 'removeuser']:
                if not args:
                    response = "{} needs a username".format(command)
                else:
                    username = args.pop(0).lower()
                    if command == 'adduser':
                        event.add_username(user, username, " ".join(args))
                    else:
                        event.remove_username(user, username)

            else:
                response = "{} invalid command, try /event help".format(command)
    except (PermissionDenied, ObjectDoesNotExist, TimestringInvalid) as err:
        logger.error(err)
        response = str(err)

    if not response and event:
        response = _show_event(event)
    update.message.reply_text(response, parse_mode=ParseMode.HTML, quote=False)


@log_cmd
def cmd_auth(unused_bot, update, args):
    """usage:

    /auth group - request that anyone in this group may create events
    /auth owner - request that you be given permission to make events in any group
    /auth master - request that you be given permission to approve owners
    /auth iamspartacus - set yourself as the first master
    """
    try:
        if not args:
            return update.message.reply_text(inspect.getdoc(cmd_auth), parse_mode=ParseMode.HTML)
        user = _get_user(update)
        chat = _get_chat(update)

        command = args[0].lower()
        if command == 'iamspartacus':
            user.iamspartacus()
            return update.message.reply_text("Hail Spartacus!")

        elif command in ['master', 'owner']:
            UserRequest.add(user=user, permission=command, reason=" ".join(args))
            return update.message.reply_text("Your request for {} access is pending", command)

        elif command == 'chat':
            chat.authorize(user)
            return update.message.reply_text("Anyone in this group may create events")

        update.message.reply_text("Unknown auth command")
    except PermissionDenied as err:
        logger.error(err)
        update.message.reply_text(str(err))


def cmd_bless(unused_bot, update, args):
    """usage:

    /bless <username> - bless a user's request
    """
    user = _get_user(update)
    name = " ".join(args)
    if not name:
        return update.message.reply_text("\n".join([
            "{} requested {} for {} at {}".format(req.user, req.permission,
                                                  req.reason, req.modified)
            for req in UserRequest.list_requests(user)]))

    try:
        req = UserRequest().get_by_username(name)
        if not req:
            return update.message.reply_text("No pending requests for {}", name)
        newperm = req.bless(user)
        update.message.reply_text("{}'s now a(n) {}".format(name, newperm))
    except (PermissionDenied, ObjectDoesNotExist) as err:
        logger.error(err)
        update.message.reply_text(str(err))


def cmd_help(unused_bot, update):
    """/help command processor"""
    # update.message.reply_text("try /signup help or /event help")
    update.message.reply_text("try /event help")


def tg_location(unused_bot, update):
    """Store any copies of the user's location in local memory"""
    msg = update.message
    user = _get_user(update)
    TGLocation.add(user=user, lat=msg.location.latitude, lng=msg.location.longitude)


def tg_unknown(unused_bot, update):
    """Unknown message handler"""
    update.message.reply_text("unknown command, try /event help")


def tg_error(unused_bot, update, error):
    """error handler for state machine"""
    if update:
        logger.warning("%s: %s", error, update)
    else:
        logger.warning("%s", error)


def workhorse(api_token):
    """Handle the actual bot processing"""
    updater = Updater(token=api_token)

    # once an hour, clean up the MWT
    updater.job_queue.run_repeating(job_mwt_cleanup, timedelta(hours=1))

    disp = updater.dispatcher
    disp.add_handler(CommandHandler(("event", "signup"), cmd_event, pass_args=True))
    disp.add_handler(CommandHandler(("start", "help"), cmd_help))
    disp.add_handler(CommandHandler("auth", cmd_auth, pass_args=True))
    disp.add_handler(CommandHandler("bless", cmd_bless, pass_args=True))
    # disp.add_handler(MessageHandler(Filters.location, tg_location))
    # disp.add_handler(MessageHandler(Filters.command, tg_unknown))
    # disp.add_error_handler(tg_error)
    updater.start_polling()
    updater.idle()
    updater.stop()


def run_from_manage(*unused_args, **unused_kwargs):
    """Executed from manage.py (see management/commands/)"""

    logging.basicConfig(
        level=logging.DEBUG,
        datefmt='%Y-%m-%d %H:%M:%S',
        format='%(asctime)s %(levelname)s %(name)s.%(funcName)s: %(message)s')
    logging.getLogger('telegram').setLevel(logging.INFO)
    logging.getLogger('JobQueue').setLevel(logging.INFO)

    workhorse(settings.TELEGRAM_BOT_API_TOKEN)
