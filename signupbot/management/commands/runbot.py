from django.core.management.base import BaseCommand, CommandError
from signupbot.bot import run_from_manage

class Command(BaseCommand):
    help = "Run the Signup Telegram robot"

    def handle(self, *args, **options):
        run_from_manage(*args, **options)
