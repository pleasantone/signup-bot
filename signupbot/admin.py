"""
Administrative menus
"""

from django.contrib import admin

from .models import TGUser, Chat, Event, Attendee, UserRequest, TGLocation


class UserRequestInline(admin.TabularInline):
    model = UserRequest
    verbose_name = "Permission Request"
    verbose_name_plural = "Permission Request"


class UserLocationInline(admin.TabularInline):
    model = TGLocation
    verbose_name = "Last Location"
    verbose_name_plural = "Last Location"


class AttendanceInline(admin.TabularInline):
    model = Attendee
    verbose_name = "Attending"
    verbose_name_plural = "Attending"


@admin.register(TGUser)
class TGUserAdmin(admin.ModelAdmin):
    """Admin menu"""
    inlines = (UserRequestInline, UserLocationInline, AttendanceInline,)
    name = "Telegram User"
    verbose_name = "Telegram Users"
    list_display = ('name', 'permission', 'modified')
    search_fields = ('permission', 'username', 'first_name', 'last_name')
    readonly_fields = ('name', 'user_id', 'username', 'first_name', 'last_name', 'language_code',
                       'created', 'modified')
    fields = ('name', 'user_id', 'permission', 'created', 'modified')

    def name(self, obj):
        return str(obj)
    name.short_description = 'Name'

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super().get_inline_instances(request, obj)


@admin.register(Chat)
class ChatAdmin(admin.ModelAdmin):
    """Admin menu"""
    list_display = ('__str__', 'type', 'allow_all')
    search_fields = ('title', 'username', 'first_name', 'last_name')
    readonly_fields = ('chat_id', 'title', 'username', 'first_name', 'last_name',
                       'type', 'invite_link', 'created', 'modified')
    fields = ('allow_all', 'masters') + readonly_fields

    def name(self, obj):
        return str(obj)
    name.short_description = 'Title'


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    """Admin menu"""
    inlines = (AttendanceInline,)
    list_display = ('slug', 'chat', 'owner', 'state', 'start_time')
    search_fields = ('chat', 'slug', 'owner')
    readonly_fields = ('created', 'modified')
    date_heirarchy = 'start_time'
