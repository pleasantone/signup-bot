"""
SignupBot and Auth System models
"""

from datetime import timedelta

from django.core.exceptions import PermissionDenied
from django.db import models, IntegrityError
from django.db.models import Q
from django.utils import timezone
from django_extensions.db.models import TimeStampedModel

_PERMISSION_LENGTH = 40             # maximum size of permission string
_STALE_LOCATION = 5                 # minutes until we think the last location was stale
_DEFAULT_USER_PRIV = 'standard'     # None = user may not create events in this channel

# pylint: disable=missing-docstring


class TGUser(TimeStampedModel):
    """Telegram User"""
    PERMISSIONS = (
        ('blocked', 'Blocked'),
        ('standard', 'Standard'),
        ('botmaster', 'Bot Master'),
    )
    user_id = models.BigIntegerField("User ID", unique=True)
    username = models.CharField(max_length=64, null=True, blank=True, default='')
    first_name = models.CharField(max_length=64, null=True, blank=True, default='')
    last_name = models.CharField(max_length=64, null=True, blank=True, default='')
    language_code = models.CharField(max_length=20, null=True, blank=True, default='')
    permission = models.CharField(max_length=_PERMISSION_LENGTH,
                                  choices=PERMISSIONS, default='standard')

    class Meta:
        ordering = ['username']
        verbose_name = 'Telegram User'

    def __str__(self):
        return (self.username
                or "{} {}".format(self.first_name, self.last_name).strip()
                or str(self.user_id))

    def update(self, **kwargs):
        for key, val in kwargs.items():
            getattr(self, key)              # raise an AttributeError if the key is bad
            setattr(self, key, val)
        self.save()
        return self

    @classmethod
    def has_master(cls):
        return cls.objects.filter(permission='botmaster').exists()

    def iamspartacus(self):
        if self.has_master() and self.permission != 'botmaster':
            raise PermissionDenied("You are not Spartacus!")
        self.permission = 'botmaster'
        self.save()


class Chat(TimeStampedModel):
    """Chat

    allow_all = anyone can create an event here
    allow_authorizer = person who set allow-all
    """
    chat_id = models.BigIntegerField("Chat ID", unique=True)
    title = models.CharField(max_length=255, null=True, blank=True)
    first_name = models.CharField(max_length=64, null=True, blank=True, default='')
    last_name = models.CharField(max_length=64, null=True, blank=True, default='')
    username = models.CharField(max_length=64, null=True, blank=True, default='')
    type = models.CharField(max_length=20)
    invite_link = models.CharField(max_length=255, null=True, blank=True, default='')
    allow_all = models.BooleanField(default=True)
    masters = models.ManyToManyField(TGUser)

    class Meta:
        ordering = ('chat_id',)

    def __str__(self):
        return (self.title
                or "{} {}".format(self.first_name, self.last_name).strip()
                or str(self.chat_id))

    def authorize(self, user):
        if user.permission != 'botmaster':
            raise PermissionDenied("{} is not allowed to authorize this chat".format(user))
        self.allow_all = True
        self.save()

    def update(self, **kwargs):
        for key, val in kwargs.items():
            getattr(self, key)              # raise an AttributeError if the key is bad
            setattr(self, key, val)
        self.save()
        return self

    def current_events(self):
        return self.event_set.filter(
            (Q(start_time=None) | Q(start_time__gt=timezone.now() - timedelta(days=1))),
            state__in=['active', 'pause'],
        )


class Event(TimeStampedModel):
    STATE = (
        ('active', 'Active'),
        ('pause', 'Paused'),
    )
    chat = models.ForeignKey(Chat, on_delete=models.CASCADE)
    owner = models.ForeignKey(TGUser, on_delete=models.PROTECT)
    slug = models.SlugField()
    title = models.CharField(max_length=100, blank=True)
    description = models.TextField(blank=True)
    start_time = models.DateTimeField(null=True, blank=True, default=None)
    duration = models.DurationField(null=True, blank=True, default=None)
    state = models.CharField(max_length=20, default='active', choices=STATE)

    class Meta:
        ordering = ('chat', 'start_time', 'slug')
        unique_together = ('chat', 'slug')

    def __str__(self):
        return "{}/{}".format(self.chat, self.slug)

    @classmethod
    def check_create(cls, chat, user):
        """User may modify (or if event isn't present, create, an event)"""
        if chat.allow_all and user.permission != 'blocked':
            return
        if user.permission == 'botmaster':
            return
        if chat.masters.filter(user_id=user.user_id).exists():
            return
        raise PermissionDenied("{}: {} may not create events here".format(chat, user))

    @classmethod
    def add(cls, chat, slug, user, **kwargs):
        """Create an event"""
        cls.check_create(chat, user)
        try:
            event = cls(chat=chat, slug=slug, owner=user, **kwargs)
            event.save()
        except IntegrityError:
            raise PermissionDenied("{}: already exists, delete the old one first".format(slug))
        return event

    def check_modify(self, user):
        if (self.owner == user
                or user.permission == 'botmaster'
                or self.chat.masters.filter(user_id=user.user_id).exists()):
            return
        raise PermissionDenied("{}: may not be modified by {}".format(self, user))

    def remove(self, user):
        """Delete this event"""
        self.check_modify(user)
        self.delete()

    def update(self, user, **kwargs):
        """Update this event"""
        self.check_modify(user)
        for key, val in kwargs.items():
            setattr(self, key, val)
        self.save()

    def add_username(self, user, target_username, comment):
        self.check_modify(user)
        target = TGUser.objects.get(username__iexact=target_username)
        return Attendee.add(self, target, comment)

    def remove_username(self, user, target_username):
        self.check_modify(user)
        self.attendee_set.filter(user__username__iexact=target_username).delete()


class Attendee(TimeStampedModel):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    user = models.ForeignKey(TGUser, on_delete=models.CASCADE)
    comment = models.CharField("Status", max_length=255, blank=True)

    class Meta:
        unique_together = ('event', 'user')
        ordering = ('event', 'created',)

    def __str__(self):
        return "{}/{}".format(self.event, self.user)

    @classmethod
    def add(cls, event, user, comment):
        attendee, _ = cls.objects.update_or_create(event=event, user=user)
        attendee.comment = comment
        attendee.save()
        return attendee


class UserRequest(TimeStampedModel):
    user = models.OneToOneField(TGUser, on_delete=models.CASCADE)
    permission = models.CharField(max_length=_PERMISSION_LENGTH)
    reason = models.CharField(max_length=1000)

    class Meta:
        ordering = ['-modified']

    def __str__(self):
        return "{}/{}".format(self.user.username, self.permission)

    def bless(self, user):
        if user.permission != 'botmaster':
            raise PermissionDenied("You may not bless requests")
        permission = self.user.permission
        self.user.permission = permission
        self.user.save()
        self.delete()
        return permission

    @classmethod
    def list_requests(cls, user):
        if user.permission != 'botmaster':
            raise PermissionDenied("You may not list requests")
        return cls.objects.all()

    @classmethod
    def get_by_name(cls, username):
        """Look up user by username"""
        return cls.objects.get(user__username=username)

    @classmethod
    def add(cls, user, permission, reason=None):
        request, _ = cls.objects.update_or_create(user=user)
        request.permission = permission
        request.reason = reason
        request.save()
        return request


class TGLocation(TimeStampedModel):
    """Last location value that user sent out"""
    user = models.OneToOneField(TGUser, on_delete=models.CASCADE)
    lat = models.FloatField()
    lng = models.FloatField()

    class Meta:
        ordering = ['-modified']

    def __str__(self):
        return "{}({},{})".format(self.user.username, self.lat, self.lng)

    @classmethod
    def get_current(cls, user):
        """Get sniffed current location"""
        threshold = timezone.now() - timedelta(minutes=_STALE_LOCATION)
        return cls.objects.get(user=user, updated__gt=threshold)

    @classmethod
    def add(cls, user, lat, lng):
        location, _ = cls.objects.update_or_create(user=user)
        location.lat = lat
        location.lng = lng
        location.save()
        return location
