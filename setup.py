import os
import sys

from setuptools import setup, find_packages

if sys.version_info < (3, 5):
    raise RuntimeError("requires Python 3.5 or newer")

BASE_DIR = os.path.abspath(os.path.dirname(__file__))

with open(os.path.join(BASE_DIR, 'README.rst'), encoding='utf-8') as stream:
    LONG_DESCRIPTION = stream.read()

setup(
    name='signupbot',
    version='0.0.1',
    description="Telegram BOT to manage project signups",
    long_description=LONG_DESCRIPTION,
    url='https://gitlab.com/pleasantone/signupbot',

    author='Paul Traina',
    author_email='mail@github.st.pst.org',
    license="GNU Affero General Public License v3",

    packages=find_packages(exclude=['tests']),
    install_requires=[
        'django',
        'django-environ',
        'django_extensions',
        'python-telegram-bot',
        'timestring-pleasantone',
        'humanize',
    ],
    include_package_data = True,
    extras_require={
        'dev': ['check-manifest'],
        'test': ['coverage'],
    },
    entry_points={
        'console_scripts': [
            'signupbot=signupbot.bot:standalone',
        ],
    },
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Environment :: Console",
        "Environment :: No Input/Output (Daemon)",
        "License :: OSI Approved :: GNU Affero General Public License v3",
        "Natural Language :: English",
        "Operating System :: POSIX",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: 3.6",
        "Topic :: Communications :: Chat",
    ],
)
